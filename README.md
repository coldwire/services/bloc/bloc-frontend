# bloc-frontend

frontend server for Bloc

## Requirements

- NodeJS >= 16
- npm or yarn

## Quickstart

### For Development

```sh
(bloc) $ yarn install
(bloc) $ yarn run dev --mode development
```

Add environment variable in this file to target your Bloc REST API domain

```sh
echo "VITE_BASE_API_URL=http://localhost:8080" > .env.development.local
```

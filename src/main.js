import { createApp } from 'vue'
import router from './router'
import { createPinia } from 'pinia'
import App from './App.vue'

const pinia = createPinia()       // Create Pinia state store instance
const app = createApp(App)        // Create Vue App instance
app.config.performance = true
app.use(pinia)                    // Attach Pinia instance plugin to Vue App
app.use(router)                   // Attach Router definition to Vue App
app.mount('#app')      // Mount Vue application

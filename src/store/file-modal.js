import { defineStore } from 'pinia'

export const useFileModal = defineStore('file-modal', {
  state: () => ({
    modalMap: {}
  }),
  getters: {
    modal: (state) => {
      return state.modalMap
    }
  },
  actions: {
    showFileModal (context, payload) {
      this.modalMap[context] = payload
    },
    hideFileModal (context) {
      delete this.modalMap[context]
    }
  }
})

import { defineStore } from 'pinia'

export const useFileContextMenu = defineStore('file-context-menu', {
  state: () => ({
    targetedBloc: {},
    eventType: null
  }),
  getters: {
    bid: (state) => {
      return state.targetedBloc.id ?? null
    }
  },
  actions: {

    /**
     * This function manage display of file context menu with 'button' and 'contextmenu' events in same time
     * @param event the javascript $event
     * @param targetedBloc the bloc entire properties
     */
    show (event, targetedBloc) {
      event.preventDefault()
      const fileContextMenu = document.getElementById('file-context-menu')
      if (targetedBloc) {
        this.targetedBloc = targetedBloc
        fileContextMenu.style.top = `${event.clientY}px`
        fileContextMenu.style.left = `${event.clientX}px`
        this.eventType = event.type
        if (event.type === 'contextmenu') {
          fileContextMenu.classList.add('visible')
          document.addEventListener('click', (event) => {
            event.preventDefault()
            const isClickInside = fileContextMenu.contains(event.target)
            if (!isClickInside) {
              fileContextMenu.classList.remove('visible')
            }
          }, { once: true })
        } else {
          fileContextMenu.classList.toggle('visible')
        }
      }
    }
  }
})

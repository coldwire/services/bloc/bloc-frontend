// TODO: Rename action 'fillFixtures' by 'loadFixtures'
import { defineStore } from 'pinia'
import blocFixtureJson from '@/fixture/bloc-root.json'

export const useBlocStore = defineStore('bloc', {
  state: () => ({
    currentBlocCursor: null,
    currentBlocList: [],
    currentBlocMap: {},
    starredBlocList: []
  }),
  getters: {
    blocs: (state) => {
      let blocList = []
      state.currentBlocList.forEach(e => {
        blocList.push(state.currentBlocMap[e])
      })
      return blocList
    },
    files: (state) => {
      let fileList = []
      state.currentBlocList.forEach(e => {
        if (!state.currentBlocMap[e].is_directory) {
          fileList.push(state.currentBlocMap[e])
        }
      })
      return fileList
    },
    directories: (state) => {
      let directoryList = []
      state.currentBlocList.forEach(e => {
        if (state.currentBlocMap[e].is_directory) {
          directoryList.push(state.currentBlocMap[e])
        }
      })
      return directoryList
    },
    starreds: (state) => {
      return state.starredBlocList
    },
    blocsCount: (state) => {
      return state.currentBlocList.length
    }
  },
  actions: {
    fillFixtures () {
      this.currentBlocCursor = blocFixtureJson.content.current_bloc_cursor
      this.currentBlocMap = blocFixtureJson.content.current_bloc_map
      this.currentBlocList = blocFixtureJson.content.current_bloc_list
      this.starredBlocList = this.currentBlocList.filter(e => this.currentBlocMap[e].is_favorite)
    },
    rename (bid, newName) {
      try {
        this.currentBlocMap[bid].name = newName
      } catch (err) {
        console.warn('failed to rename bloc, error : ' + err.message)
      }
    },
    move (from, toDirectory) {
      // call api (try - catch) //
      // TODO: fetch api call to move file or folder here
      console.log('move file (' + from + ') in directory (' + toDirectory + ')')
      // TODO: remove file or directory from 'currentBlocMap' and 'currentBlocList'
      this.delete(from)
    },
    toggleFavorite (bid) {
      try {
        if (!this.currentBlocMap[bid].is_directory) {
          if (this.currentBlocMap[bid].is_favorite) {
            for (let i = 0; i < this.starredBlocList.length; i++) {
              if (this.starredBlocList[i] === bid) {
                this.starredBlocList.splice(i, 1)
                break
              }
            }
            this.currentBlocMap[bid].is_favorite = false
          } else {
            this.starredBlocList.unshift(bid)
            this.currentBlocMap[bid].is_favorite = true
          }
        } else {
          console.warn('cannot toggle favorite a directory for the moment')
        }
      } catch (err) {
        console.warn('failed to toggle favorite bloc, error : ' + err.message)
      }
    },
    delete (bid) {
      try {
        if (this.currentBlocMap[bid]) {
          // Remove bloc from list array
          for (let i = 0; i < this.currentBlocList.length; i++) {
            if (this.currentBlocList[i] === bid) {
              this.currentBlocList.splice(i, 1)
              break
            }
          }
          // Remove bloc from favorite if exist
          if (!this.currentBlocMap[bid].is_directory) {
            if (this.currentBlocMap[bid].is_favorite) {
              for (let x = 0; x < this.starredBlocList.length; x++) {
                if (this.starredBlocList[x] === bid) {
                  this.starredBlocList.splice(x, 1)
                  break
                }
              }
            }
          }
          // Remove bloc from global mapping
          delete this.currentBlocMap[bid]
        }
      } catch (err) {
        console.log('failed to remove bloc, error : ' + err.message)
      }
    }
  }
})

# Rules to implement CSS class in this project

This guide explain how and when it is necessary to use SCSS global 
class definition in this project.

> Generally you implement a CSS code in VueJS components directly but
> sometimes you can reach the limit of **DRY** (Don't Repeat Yourself) 
> features offered by VueJS components.

## When can I use CSS class ?

  - Lorsque vous prévoyer d'utiliser les classes CSS dans divers composants à l'avenir.
  - Lorsque vous devez faire des rituels sombre au niveau de votre composant VueJS.
  - Lorsque le style de votre composant VueJS devient beaucoup trop grand.

## How can I use CSS class ?

### 1 - Séparer les fichiers SCSS en composants visuels simple

Les fichiers doivent avoir des noms simples, maximum 2 mots. S'il y a plus
d'un mot alors ils seront séparés par un tiret "**-**".

Le dossier **./src/asset/scss** est linéaire, il n'y a aucun dossier créer
dedans. Seulement des fichiers SCSS. 

Le projet démarre avec des fichiers SCSS de base tel que :
  - **font.scss** : s'occupe de charger la police d'écriture
  - **variable.scss** : définit les variables SCSS global du projet
  - **reset.scss** : écrase la configuration par défaut du css et adapte les éléments de façon global
  - **layout.scss** : définit quelques classes pour les mises en pages répétitives tel que flex par exemple

Pour charger les fichiers SCSS de façon global dans les balises de styles VueJS nous importeront comme ci-dessous
les fichiers scss dans le fichier de configuration Vite (**vite.config.js**):

```js
css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
        @import "@/asset/scss/font.scss";
        @import "@/asset/scss/variable.scss";
        @import "@/asset/scss/reset.scss";
        @import "@/asset/scss/layout.scss";
        @import "@/asset/scss/button.scss";
        @import "@/asset/scss/notification.scss";
        @import "@/asset/scss/card.scss";
        @import "@/asset/scss/context-menu.scss";`
      }
    }
  }
```

*A noter que les fichiers seront charger de haut en bas (ordre de priorité)*

### 2 - Rules of class definition

La méthodologie **BEM** dans les fichiers SCSS est fortement recommandé afin
de réduire les collisions de noms de classes et pour facilement débugger
en utilisant les outils de développement. [Voici un lien pour comprendre comment
utiliser **BEM**](https://www.alticreation.com/bem-pour-le-css)

Si votre nom de fichier est **notification.scss** alors nous auront des classes
comme ceci :

```scss
.notification-base {}     // fortement recommandé de le définir pour un composant CSS
.notification-success {}
.notification-info {}
.notification-warning {}
.notification-danger {}
.notification-indeterminate {}
```

The **.\<file name\>-base {}** correspond à la forme la plus simplifié de notre
composant. Donc pour creer un composant complet nous l'utiliserons par exemple
comme ceci :

```html
<template>
  <div class="notification-base notification-success"></div>
</template>
```

Les suffix **success**, **info**, **warning**, **danger** sont des exemples
hérité des methodes de style **Bootstrap** pour illustrer l'exemple.

## Dernier mots

Ce sera tout pour les regles d'implementations. Gardez tout de fois à l'esprit
que si vous souhaiter ajouter un bouton par exemple. Il faudra d'abord regarder
si un fichier SCSS l'implémente sinon il faudra utiliser le composant VueJS.
Donc le fichier SCSS est prioritaire. Si aucun ne l'implémente il faudra alors
réfléchir à la meilleur façon de le faire (composant SCSS / composant VueJS).

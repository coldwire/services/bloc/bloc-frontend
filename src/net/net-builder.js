'use strict';
import router from '@/router/index'

export function CustomFetch(url, payload) {
  return new Promise((resolve, reject) => {
    fetch(
      import.meta.env.VITE_BASE_API_URL + url,
      payload
    ).then(response => {
      // response only can be ok in range of 2XX
      if (response.ok) {
        // you can call response.json() here too if you want to return json
        resolve(response)
      } else {
        //handle errors in the way you want to
        switch (response.status) {
          case 402:
            console.warn('Authentication required')
            router.push({name: 'auth.login'})
            break
          case 403:
            console.warn('Forbidden resource')
            router.push({name: 'auth.login'})
            break
          case 404:
            console.warn('Request failed (404 HTTP code)')
            break
          case 500:
            console.warn('Request failed internal api server error (500 HTTP code)')
            break
          default:
            console.log('Some error occured')
            break
        }
        //here you also can throw custom error too
        reject(response)
      }
    })
      .catch(error => {
        //it will be invoked mostly for network errors
        //do what ever you want to do with error here
        console.log(error)
        reject(error)
      })
  })
}

export class NetFetchBuilder {
  constructor () {
    this.contentType = null
    this.headers = {}
    this.method = 'GET'
    this.body = {}
    this.mode = null
    this.config = {}
  }
  setMethod (method) {
    this.method = method
    return this
  }
  addHeaderContentJSON () {
    this.contentType = 'JSON'
    this.headers['Content-Type'] = 'application/json; charset=utf-8'
    return this
  }
  setBody (payload) {
    this.body = payload
    return this
  }
  setCORS (isActive) {
    isActive ? this.mode = 'cors' : this.mode = undefined
    return this
  }
  build (url) {
    // encode Body
    switch (this.contentType) {
      case 'JSON':
        this.body = JSON.stringify(this.body)
        break
    }

    // load config
    this.config = {
      method: this.method,
      headers: this.headers,
      body: this.body,
      mode: this.mode
    }
    if (!this.mode) { delete this.config.mode }
    if (this.method !== 'POST') { delete this.config.body }

    // return async request handler
    return new CustomFetch(
      url,
      this.config
    )
  }
}

// export class NetXHRBuilder {}

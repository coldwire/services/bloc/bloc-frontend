'use strict';
import { NetFetchBuilder } from '@/net/net-builder'

export class NetBloc {
  static uploadStart(name, parent) {
    return new NetFetchBuilder()
      .setMethod('POST')
      .addHeaderContentJSON()
      .setBody({
        name: name,
        parent: parent
      })
      .build('/bloc/upload/start')
  }

  static uploadStop(uid, mime, key, nonce) {
    return new NetFetchBuilder()
      .setMethod('POST')
      .addHeaderContentJSON()
      .setBody({
        mime_type: mime,
        key: key,
        nonce: nonce
      })
      .build(`/bloc/upload/stop/${uid}`)
  }

  // TODO:
  // static uploadAbort() {}

  static uploadWrite(uid, chunkId, chunkData) {
    return new NetFetchBuilder()
      .setMethod('PUT')
      .setBody(chunkData)
      .build(`/bloc/upload/${uid}/${chunkId}`)
  }

  static info(uid) {
    return new NetFetchBuilder()
      .setMethod('GET')
      .build(`/bloc/${uid}`)
  }

  static download(uid, chunkId) {
    return new NetFetchBuilder()
      .setMethod('GET')
      .build(`/bloc/download/${uid}/${chunkId}`)
  }
}
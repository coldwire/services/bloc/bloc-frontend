'use strict';
import { NetFetchBuilder } from '@/net/net-builder'

export class NetUser {
  static info() {
    return new NetFetchBuilder()
      .setMethod('GET')
      .build('/user/info')
  }
}

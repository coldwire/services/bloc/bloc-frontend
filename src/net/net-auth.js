'use strict';
import { NetFetchBuilder } from '@/net/net-builder'

export class NetAuth {
  static register (payload) {
    return new NetFetchBuilder()
      .setMethod('POST')
      .addHeaderContentJSON()
      .setBody(payload)
      .build('/auth/register')
  }
  static login (payload) {
    return new NetFetchBuilder()
      .setMethod('POST')
      .addHeaderContentJSON()
      .setBody(payload)
      .build('/auth/login')
  }
  static logout () {
    return new NetFetchBuilder()
      .setMethod('GET')
      .build('/auth/logout')
  }
}

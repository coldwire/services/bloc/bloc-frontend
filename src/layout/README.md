# Layout Directory

## EN

This directory contain layout components. This layout components contain generally
one or more **<slot \/>** element and are static section page frequently used. The
**Layout** prefix is needed.

## FR

Ce dossier contient des composants de mise en page. Les composants de mise en page
contiennent généralement une ou plusieurs balise **<slot \/>** et correspondent à 
la fois à des sections de page static fréquemment utilisé. Le préfix **Layout** est 
obligatoire devant chacun.
